//! The **sequencer** module provides a trait for working with **Iterator**s that yield **OscMessage**s.
//! Along with the **Iterator** trait, **OscMessageSequencer** provides **delay**, which returns
//! the amount of time a client should wait between sending **OscMessage**s.
//! The module also provides implementations of the **OscMessageSequencer** trait for common
//! useful waveforms.

use crate::CvAddress;
use crate::CvMessage;
use crate::OscMessage;
use sample::signal;
use sample::signal::ConstHz;
use sample::signal::Signal;
use std::time;

/// Types that yield **OscMessage**s and provide a delay to
/// pause between messages.
pub trait OscMessageSequencer: Iterator<Item = OscMessage> {
    fn delay(&self) -> time::Duration;
}

/// **NoiseSequencer** generates a sequence of **CvMessage**s
/// representing random noise.
/// ```rust
/// use open_sound_module::OscMessageSequencer;
/// use open_sound_module::NoiseSequencer;
/// use open_sound_module::CvAddress;
/// use std::time;
///
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
///     let seconds = time::Duration::from_secs(1);
///     let mut noise = NoiseSequencer::new(CvAddress::A, 0, 4.0, seconds);
///     let _msg = noise.next();
///     let _delay = noise.delay();
///     Ok(())  
/// }
/// ```
pub struct NoiseSequencer {
    addr: CvAddress,
    noise: signal::Noise,
    rate: f64,
    duration: time::Duration,
    start: time::Instant,
}

impl NoiseSequencer {
    /// Create a new **NoiseSequencer** from a **CvAddress**, a random
    /// seed, a rate, and a duration. The rate is a sample rate that
    /// is used to determine the delay between messages. For
    /// exampe, a sample rate of 4.0 will cause the OscSeq
    /// to wait 250ms between messages for a rate of 4
    /// messages a second.
    pub fn new(addr: CvAddress, seed: u64, rate: f64, duration: time::Duration) -> NoiseSequencer {
        let noise = signal::noise(seed);
        let start = time::Instant::now();
        NoiseSequencer {
            addr: addr,
            noise: noise,
            rate: rate,
            duration: duration,
            start: start,
        }
    }
}

impl Iterator for NoiseSequencer {
    type Item = OscMessage;

    /// Yield the next message in the sequence.
    fn next(&mut self) -> Option<Self::Item> {
        let now = time::Instant::now();
        if now - self.start >= self.duration {
            return None;
        }
        let frame = self.noise.next();
        let msg = CvMessage::new(self.addr.clone(), frame[0] as f32);
        Some(OscMessage::CvMessage(msg))
    }
}

impl OscMessageSequencer for NoiseSequencer {
    /// Return the duration the sequencer should wait between messages.
    fn delay(&self) -> time::Duration {
        let ms = time::Duration::from_millis(1000 / self.rate as u64);
        ms
    }
}

/// **SawSequencer** generates a sequence of **CvMessage**s
/// representing a saw wave.
/// ```rust
/// use open_sound_module::OscMessageSequencer;
/// use open_sound_module::SawSequencer;
/// use open_sound_module::CvAddress;
/// use std::time;
///
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
///     let seconds = time::Duration::from_secs(1);
///     let mut saw = SawSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
///     let _msg = saw.next();
///     let _delay = saw.delay();
///     Ok(())  
/// }
/// ```
pub struct SawSequencer {
    addr: CvAddress,
    saw: signal::Saw<ConstHz>,
    rate: f64,
    duration: time::Duration,
    start: time::Instant,
}

impl SawSequencer {
    /// Create a new **SawSequencer** from a **CvAddress**, a rate
    /// a hertz and a duration. The rate is a sample rate that
    /// is used to determine the delay between messages. For
    /// exampe, a sample rate of 4.0 will cause the sequencer
    /// to wait 250ms between messages for a rate of 4
    /// messages a second.
    pub fn new(addr: CvAddress, rate: f64, hz: f64, duration: time::Duration) -> SawSequencer {
        let saw = signal::rate(rate).const_hz(hz).saw();
        let start = time::Instant::now();
        SawSequencer {
            addr: addr,
            saw: saw,
            rate: rate,
            duration: duration,
            start: start,
        }
    }
}

impl Iterator for SawSequencer {
    type Item = OscMessage;

    /// Yield the next message in the sequence.
    fn next(&mut self) -> Option<Self::Item> {
        let now = time::Instant::now();
        if now - self.start >= self.duration {
            return None;
        }
        let frame = self.saw.next();
        let msg = CvMessage::new(self.addr.clone(), frame[0] as f32);
        Some(OscMessage::CvMessage(msg))
    }
}

impl OscMessageSequencer for SawSequencer {
    /// Return the duration the sequence should wait between messages.
    fn delay(&self) -> time::Duration {
        let ms = time::Duration::from_millis(1000 / self.rate as u64);
        ms
    }
}

/// **SineSequencer** generates a sequence of **CvMessage**s
/// representing a sine wave.
/// ```rust
/// use open_sound_module::OscMessageSequencer;
/// use open_sound_module::SineSequencer;
/// use open_sound_module::CvAddress;
/// use std::time;
///  
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
///     let seconds = time::Duration::from_secs(1);
///     let mut sine = SineSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
///     let _msg = sine.next();
///     let _delay = sine.delay();
///     Ok(())  
/// }
/// ```
pub struct SineSequencer {
    addr: CvAddress,
    sine: signal::Sine<ConstHz>,
    rate: f64,
    duration: time::Duration,
    start: time::Instant,
}

impl SineSequencer {
    /// Create a new **SineSequencer** from a **CvAddress**, a rate
    /// and a hertz. The rate is a sample rate that
    /// is used to determine the delay between messages. For
    /// exampe, a sample rate of 4.0 will cause the OscSeq
    /// to wait 250ms between messages for a rate of 4
    /// messages a second.
    pub fn new(addr: CvAddress, rate: f64, hz: f64, duration: time::Duration) -> SineSequencer {
        let sine = signal::rate(rate).const_hz(hz).sine();
        let start = time::Instant::now();
        SineSequencer {
            addr: addr,
            sine: sine,
            rate: rate,
            duration: duration,
            start: start,
        }
    }
}

impl Iterator for SineSequencer {
    type Item = OscMessage;

    /// Yield the next message in the sequence.
    fn next(&mut self) -> Option<Self::Item> {
        let now = time::Instant::now();
        if now - self.start >= self.duration {
            return None;
        }
        let frame = self.sine.next();
        let msg = CvMessage::new(self.addr.clone(), frame[0] as f32);
        Some(OscMessage::CvMessage(msg))
    }
}

impl OscMessageSequencer for SineSequencer {
    /// Return the duration the sequence should wait between messages.
    fn delay(&self) -> time::Duration {
        let ms = time::Duration::from_millis(1000 / self.rate as u64);
        ms
    }
}

/// **SquareSequencer** generates a sequence of **CvMessage**s
/// representing a square wave.
/// ```rust
/// use open_sound_module::OscMessageSequencer;
/// use open_sound_module::SquareSequencer;
/// use open_sound_module::CvAddress;
/// use std::time;
///  
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
///     let seconds = time::Duration::from_secs(1);
///     let mut square = SquareSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
///     let _msg = square.next();
///     let _delay = square.delay();
///     Ok(())  
/// }
/// ```
pub struct SquareSequencer {
    addr: CvAddress,
    square: signal::Square<ConstHz>,
    rate: f64,
    duration: time::Duration,
    start: time::Instant,
}

impl SquareSequencer {
    /// Create a new **SquareSequencer** from a **CvAddress**, a rate
    /// and a hertz. The rate is a sample rate that
    /// is used to determine the delay between messages. For
    /// exampe, a sample rate of 4.0 will cause the OscSeq
    /// to wait 250ms between messages for a rate of 4
    /// messages a second.
    pub fn new(addr: CvAddress, rate: f64, hz: f64, duration: time::Duration) -> SquareSequencer {
        let square = signal::rate(rate).const_hz(hz).square();
        let start = time::Instant::now();
        SquareSequencer {
            addr: addr,
            square: square,
            rate: rate,
            duration: duration,
            start: start,
        }
    }
}

impl Iterator for SquareSequencer {
    type Item = OscMessage;

    /// Yield the next message in the sequence.
    fn next(&mut self) -> Option<Self::Item> {
        let now = time::Instant::now();
        if now - self.start >= self.duration {
            return None;
        }
        let frame = self.square.next();
        let msg = CvMessage::new(self.addr.clone(), frame[0] as f32);
        Some(OscMessage::CvMessage(msg))
    }
}

impl OscMessageSequencer for SquareSequencer {
    /// Return the duration the sequence should wait between messages.
    fn delay(&self) -> time::Duration {
        let ms = time::Duration::from_millis(1000 / self.rate as u64);
        ms
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_noise_sequence() -> Result<(), Box<dyn std::error::Error>> {
        let seconds = time::Duration::from_secs(1);
        let mut gen = NoiseSequencer::new(CvAddress::A, 0, 4.0, seconds);
        let _msg = gen.next().unwrap();
        Ok(())
    }

    #[test]
    fn test_saw_sequence() -> Result<(), Box<dyn std::error::Error>> {
        let seconds = time::Duration::from_secs(1);
        let mut gen = SawSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
        let _msg = gen.next().unwrap();
        Ok(())
    }

    #[test]
    fn test_sine_sequence() -> Result<(), Box<dyn std::error::Error>> {
        let seconds = time::Duration::from_secs(1);
        let mut gen = SineSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
        let _msg = gen.next().unwrap();
        Ok(())
    }

    #[test]
    fn test_square_sequence() -> Result<(), Box<dyn std::error::Error>> {
        let seconds = time::Duration::from_secs(1);
        let mut gen = SquareSequencer::new(CvAddress::A, 4.0, 1.0, seconds);
        let _msg = gen.next().unwrap();
        Ok(())
    }
}
