extern crate open_sound_module;

use open_sound_module::CvAddress;
use open_sound_module::NoiseSequencer;
use open_sound_module::OscClient;
use open_sound_module::SawSequencer;
use open_sound_module::SineSequencer;
use open_sound_module::SquareSequencer;
use std::time;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "osm_cli",
    about = "Command line client for Rebel Tech Open Sound Module"
)]
struct Device {
    /// IP address of Open Sound Module
    #[structopt(short = "i", long = "ip")]
    ip: String,
    /// Port of Open Sound Module
    #[structopt(short = "p", long = "port", default_value = "8000")]
    port: String,
    #[structopt(subcommand)]
    command: Command,
    #[structopt(short = "a", long = "address")]
    addr: CvAddress,
}

#[derive(Debug, StructOpt)]
enum Command {
    #[structopt(name = "noise")]
    Noise {
        /// Set random seed
        #[structopt(short = "s", long = "seed", default_value = "0")]
        seed: u64,
        // Set sample rate
        #[structopt(short = "r", long = "rate")]
        rate: f64,
        // Set seconds
        #[structopt(short = "d", long = "duration")]
        duration_seconds: u64,
    },
    #[structopt(name = "saw")]
    Saw {
        /// Set sample rate
        #[structopt(short = "r", long = "rate")]
        rate: f64,
        /// Set step size
        #[structopt(short = "h", long = "hertz")]
        hz: f64,
        // Set seconds
        #[structopt(short = "d", long = "duration")]
        duration_seconds: u64,
    },
    #[structopt(name = "sine")]
    Sine {
        /// Set sample rate
        #[structopt(short = "r", long = "rate")]
        rate: f64,
        /// Set step size
        #[structopt(short = "h", long = "hertz")]
        hz: f64,
        // Set seconds
        #[structopt(short = "d", long = "duration")]
        duration_seconds: u64,
    },
    #[structopt(name = "square")]
    Square {
        /// Set sample rate
        #[structopt(short = "r", long = "rate")]
        rate: f64,
        /// Set step size
        #[structopt(short = "h", long = "hertz")]
        hz: f64,
        // Set seconds
        #[structopt(short = "d", long = "duration")]
        duration_seconds: u64,
    },
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let dev = Device::from_args();
    let client = OscClient::new(format!("{}:{}", dev.ip, dev.port))?;

    match dev.command {
        Command::Sine {
            rate,
            hz,
            duration_seconds,
        } => {
            let seconds = time::Duration::from_secs(duration_seconds);
            let mut seq = SineSequencer::new(dev.addr, rate, hz, seconds);
            client.send_sequence(&mut seq)?;
        }
        Command::Saw {
            rate,
            hz,
            duration_seconds,
        } => {
            let seconds = time::Duration::from_secs(duration_seconds);
            let mut seq = SawSequencer::new(dev.addr, rate, hz, seconds);
            client.send_sequence(&mut seq)?;
        }
        Command::Square {
            rate,
            hz,
            duration_seconds,
        } => {
            let seconds = time::Duration::from_secs(duration_seconds);
            let mut seq = SquareSequencer::new(dev.addr, rate, hz, seconds);
            client.send_sequence(&mut seq)?;
        }
        Command::Noise {
            seed,
            rate,
            duration_seconds,
        } => {
            let duration = time::Duration::from_secs(duration_seconds);
            let mut seq = NoiseSequencer::new(dev.addr, seed, rate, duration);
            client.send_sequence(&mut seq)?;
        }
    };

    Ok(())
}
