//! The **osc** module provides an enum that can represent a **CvMessage** or a **TriggerMessage**.
//! It also provides the **Read** trait for these messages, and a convenience
//! function for converting these messages into a vector of bytes.

use crate::CvMessage;
use crate::TriggerMessage;
use std::io::Read;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OscMessage {
    CvMessage(CvMessage),
    TriggerMessage(TriggerMessage),
}

impl OscMessage {
    /// Return a vector of u8 bytes representing the
    /// osm::Msg in Open Sound Control protocol
    pub fn to_vec(&self) -> Vec<u8> {
        let bytes = match self {
            OscMessage::CvMessage(m) => {
                let bytes = m.to_vec();
                bytes
            }
            OscMessage::TriggerMessage(m) => {
                let bytes = m.to_vec();
                bytes
            }
        };
        bytes
    }
}

impl Read for OscMessage {
    /// Provides Read Trait for OscMessage
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        let sz = match self {
            OscMessage::CvMessage(m) => {
                let sz = m.read(buf)?;
                sz
            }
            OscMessage::TriggerMessage(m) => {
                let sz = m.read(buf)?;
                sz
            }
        };
        Ok(sz)
    }
}

/// Helper function that pads a string properly for
/// open sound control protocol
pub fn write_osc_string(buf: &mut Vec<u8>, data: String) {
    buf.extend_from_slice(data.as_bytes());

    let pad = match data.len() % 4 {
        0 => 0,
        v => 4 - v,
    };

    for _i in 0..pad {
        buf.push(0u8)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_write_osc_string() -> Result<(), Box<dyn std::error::Error>> {
        let mut buf: Vec<u8> = Vec::new();
        write_osc_string(&mut buf, "123".to_string());
        assert!(buf.len() == 4);

        let mut buf: Vec<u8> = Vec::new();
        write_osc_string(&mut buf, "12345".to_string());
        assert!(buf.len() == 8);

        Ok(())
    }
}
