# open-sound-module
An unofficial library providing tools for working with Rebel Tech's [Open Sound Module](http://pingdynasty.github.io/OpenSoundModule/Instructions/).

## cv
The cv module provides types for creating and working with control voltage messages.
```rust
extern crate open_sound_module;

use std::io::Read;
use open_sound_module::{CvMessage, CvAddress};
  
fn main() -> Result<(), failure::Error> {
    let mut msg = CvMessage::new(CvAddress::A, -0.3);
    let bytes = msg.to_vec();
    let mut buf: [u8; 1024] = [0; 1024];
    let n = msg.read(&mut buf)?;
    assert!(n == 20);
    Ok(())  
}
```
## trigger
The trigger module provides types for creating and working with trigger messages.
```rust
extern crate open_sound_module;

use std::io::Read;
use open_sound_module::{TriggerMessage, TriggerAddress};
  
fn main() -> Result<(), failure::Error> {
    let mut msg = TriggerMessage::new(TriggerAddress::B, 1);
    let bytes = msg.to_vec();
    let mut buf: [u8; 1024] = [0; 1024];
    let n = msg.read(&mut buf)?;
    assert!(n == 20);
    Ok(())  
}
```
## sequencer
The sequencer module provides an OscMessageSequencer trait built on Iterator, that can be used to send a series of control voltage and trigger messages. It also includes several impementations using [sample::signal](https://docs.rs/sample/0.6.2/sample/signal/index.html) that represent some useful waveforms.
```rust
extern crate open_sound_module;

use open_sound_module::OscMessageSequence;
use open_sound_module::SineSequence;
use open_sound_module::CvAddress;
use std::time;

fn main() -> Result<(), failure::Error> {
    let rate = 4.0;
    let hz = 1.0;
    let seconds = time::Duration::from_secs(30);
    let mut noise = SineSequence::new(CvAddress::A, rate, hz, seconds);
    let _msg = noise.next();
    let _delay = noise.delay();
    Ok(())  
}
```
## client
The client module provides a Client type to simplify sending cv and trigger messages over UDP in [Open Sound Control](http://opensoundcontrol.org/introduction-osc) protocol.
```rust
extern crate open_sound_module;

use open_sound_module::Client;
use open_sound_module::CvAddress;
use open_sound_module::OscMessage;
use open_sound_module::NoiseSequence;
use std::time;

fn main() -> Result<(), failure::Error> {
  let client = Client::new("127.0.0.1:8888".to_string())?;

  let msg = CvMessage::new(CvAddress::B, -1.0);
  client.send_osc_message(OscMessage::CvMessage(msg))?;

  let seed = 0;
  let rate = 4.0;
  let seconds = time::Duration::from_secs(1);
  let mut noise = NoiseSequence::new(CvAddress::A, seed, rate, seconds);
  client.send_sequence(&mut noise)?;
  Ok(())
}
```
