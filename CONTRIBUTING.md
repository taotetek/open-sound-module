# Contributing to open-sound-module
The contributors are listed in AUTHORS (add yourself). This project uses the MPL v2 license, see LICENSE.

## Our Process
Before you send a pull request, please familiarize yourself with the [C4.1 Collective Code Construction Contract](https://rfc.zeromq.org/spec:42/C4/) process.
A quick summary (but please, do read the process document):

* A Pull Request should be described in the form of a problem statement.
* The code included with the pull request should be a proposed solution to that problem.
* The submitted code should adhere to Rust API style guidelines when possible
* The submitted code should include tests.
* The submitted code should not break any existing tests.
"A Problem" should be one single clear problem. Large complex problems should be broken down into a series of smaller problems when ever possible.
